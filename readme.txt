This project was developed using Visual Studio 2017 on .net core 1.1

build the project by running the following from the root using command prompt or powershell
[dotnet build]

Run the tests by running the following
[dotnet test .\VendingMachineTests\VendingMachineTests.csproj]

Or... Open up project in Visual Studio for all the bells and whistles!