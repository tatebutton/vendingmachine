﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine;
using VendingMachine.Models;
using VendingMachine.Models.Coins;
using VendingMachine.Models.Products;

namespace VendingMachineTests
{
    [TestClass]
    public class VendingOperationTest
    {
        private VendingOperation _vendingOperation;

        [TestInitialize]
        public void Initialize()
        {
            _vendingOperation = new VendingOperation();
        }

        [TestMethod]
        public void Test_Constructor()
        {
            Assert.IsInstanceOfType(_vendingOperation.CustomerInsertedCoins, typeof(List<Coin>));
            Assert.IsInstanceOfType(_vendingOperation.AvailableDenominations, typeof(List<Coin>));
            Assert.IsInstanceOfType(_vendingOperation.CoinReturn, typeof(List<Coin>));
            Assert.IsInstanceOfType(_vendingOperation.TellerCoins, typeof(List<Coin>));
        }

        [TestMethod]
        public void whenCoinOperationIsConstructed_AvailableCoinsWillBeRetreived()
        {
            Assert.IsNotNull(_vendingOperation.AvailableDenominations);
        }
        
        [TestMethod]
        public void whenCoinOperationIsPassedAValidDenomination_ThatDenominationIsAddedToAvailableFunds()
        {
            var validCoin = _vendingOperation.AvailableDenominations.FirstOrDefault(x => x.IsAccepted);
            _vendingOperation.AddCustomerFunds(validCoin);
            Assert.AreEqual(validCoin.DenominationInCents, _vendingOperation.CustomerAvailableFunds);
        }

        [TestMethod]
        public void whenCoinOperationIsPassedAQuarterSpecs_CoinIsValid()
        {
            var quarter = new Quarter();
            var insertedCoin = new InsertedCoin() {WeightInGrams = quarter.GramWeight, DiameterInMillemeters = quarter.MillimeterDiameter};
            Assert.IsNotNull(_vendingOperation.ValidateCoin(insertedCoin));
            Assert.AreEqual(new Quarter().GetType(), _vendingOperation.ValidateCoin(insertedCoin).GetType());
        }


        [TestMethod]
        public void whenCoinOperationIsPassedAPennySpecs_CoinIsNotValid()
        {
            var penny = new Penny();
            var insertedCoin = new InsertedCoin() { WeightInGrams = penny.GramWeight, DiameterInMillemeters = penny.MillimeterDiameter};
            Assert.AreEqual(false,_vendingOperation.ValidateCoin(insertedCoin).IsAccepted);
        }

        [TestMethod]
        public void whenValidCoinIsInserted_CoinIsAddedToFunds()
        {
            var quarter = new Quarter();
            var insertedCoin = new InsertedCoin() { WeightInGrams = quarter.GramWeight, DiameterInMillemeters = quarter.MillimeterDiameter };
            _vendingOperation.InsertCoin(insertedCoin);
            Assert.AreEqual(_vendingOperation.CustomerAvailableFunds,quarter.DenominationInCents);
        }

        [TestMethod]
        public void whenInvalidCoinIsInserted_CoinIsAddedToReturn()
        {
            var penny = new Penny();
            var insertedCoin = new InsertedCoin() { WeightInGrams = penny.GramWeight, DiameterInMillemeters = penny.MillimeterDiameter };
            _vendingOperation.InsertCoin(insertedCoin);
            Assert.AreNotEqual(_vendingOperation.CustomerAvailableFunds, penny.DenominationInCents);
            Assert.AreEqual(penny.DenominationInCents, _vendingOperation.CoinReturn.Sum(x => x.DenominationInCents));
        }

        [TestMethod]
        public void whenCoinInsertedWithDefinedProperties_PropertiesAreSet()
        {
            var insertedCoin = new InsertedCoin(weightInGrams:1,diameterInMillemeters:2);
            Assert.AreEqual(1, insertedCoin.WeightInGrams);
            Assert.AreEqual(2, insertedCoin.DiameterInMillemeters);
        }

        [TestMethod]
        public void whenVendingMachineIsLoaded_AllProductsWillBeInitialized()
        {
            _vendingOperation.LoadAllProduct();
            foreach (var product in _vendingOperation.AvailableProducts)
            {
                Assert.AreEqual(_vendingOperation.CapacityForEachProduct, _vendingOperation.ProductInventory.Count(x => x.GetType() == product.GetType()));
            }
        }

        [TestMethod]
        public void whenLoadingMachineProduct_OneProductWillBeLoaded()
        {
            var candyType = new Candy().GetType();
            _vendingOperation.LoadSingleProduct(candyType);
            Assert.AreEqual(_vendingOperation.CapacityForEachProduct, _vendingOperation.ProductInventory.Count(x => x.GetType() == candyType));
        }
        
        [TestMethod]
        public void givenInadequateFunds_DisplayWillShowInsert()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            Assert.IsFalse(_vendingOperation.HasAdaquateFunds(new Candy()));
            Assert.AreEqual(DisplayOptions.ShowInsertCoin(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void givenAdequateFunds_WillReturnTrue()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Dollar());
            Assert.IsTrue(_vendingOperation.HasAdaquateFunds(new Candy()));
        }

        [TestMethod]
        public void givenProductAvailable_WillReturnTrue()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            Assert.IsTrue(_vendingOperation.HasAvailableInventory(new Candy()));
        }
        

        [TestMethod]
        public void givenNoAvailableInventory_DisplayWillShowOutOfStock()
        {
            _vendingOperation.HasAvailableInventory(new Candy());
            Assert.AreEqual(DisplayOptions.ShowOutOfStock(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void givenAvailableInventoryAndAdequateFunds_WillVendProduct()
        {
            var candy = new Candy();
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            _vendingOperation.CustomerInsertedCoins.Add(new Dollar());
            Assert.AreEqual(candy,_vendingOperation.VendProduct(candy));
        }

        [TestMethod]
        public void givenAvailableProduct_ProductWillBeRemoved()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            var initialProductCount = _vendingOperation.ProductInventory.Count;
            _vendingOperation.RemoveProduct(new Candy());

            Assert.AreEqual(initialProductCount - 1, _vendingOperation.ProductInventory.Count);
        }

        [TestMethod]
        public void givenNoTellerFunds_WillShowExactChange()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Dollar());
            var candy = new Candy();
            Assert.IsTrue(_vendingOperation.RequiresExactChange(candy));
            Assert.AreEqual(DisplayOptions.ShowExactChange(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void givenTellerHasFunds_WillCalculateIfChangeCanBeMade()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.TellerCoins.Add(new Nickel());
            var candy = new Candy();
            Assert.IsTrue(_vendingOperation.RequiresExactChange(candy));
            Assert.AreEqual(DisplayOptions.ShowExactChange(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void givenCustomerDoesNotHaveEnoughFunds_WillCalculateIfChangeCanBeMade()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.TellerCoins.Add(new Nickel());
            var candy = new Candy();
            Assert.IsTrue(_vendingOperation.RequiresExactChange(candy));
            Assert.AreEqual(DisplayOptions.ShowInsertCoin(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void givenExcessFunds_WillPutRemainderCoinsCalculated()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.TellerCoins.Add(new Dime());
            var chips = new Candy();
            Assert.AreEqual(10,_vendingOperation.ReturnsCustomerChange(chips).Sum(x => x.DenominationInCents));
        }

        [TestMethod]
        public void givenUserHasNotInsertedAnyCoins_NoChangeWillBeReturned()
        {
            Assert.IsNull(_vendingOperation.ReturnCustomerCoins());

        }

        [TestMethod]
        public void givenUserHasEnteredCoins_ChangeWillBeDespensed()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            Assert.IsNotNull(_vendingOperation.ReturnCustomerCoins());
        }


        [TestMethod]
        public void int_givenExactChangeAndAvailableProduct_CustomerWillReceiveProduct()
        {
            _vendingOperation.LoadSingleProduct(typeof(Chips));
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            var customerPurchase = _vendingOperation.SelectProduct(new Chips());

            Assert.AreEqual(customerPurchase.Product.GetType(), typeof(Chips));
            Assert.AreEqual(DisplayOptions.ShowThankYou(), _vendingOperation.DisplayText);
            Assert.AreEqual(0, customerPurchase.ReturnedCoins.Count);
        }

        [TestMethod]
        public void int_givenExcessChangeAndAvailableProduct_CustomerWillReceiveProductAndChange()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.TellerCoins.Add(new Dime());

            var customerPurchase = _vendingOperation.SelectProduct(new Candy());

            Assert.AreEqual(customerPurchase.Product.GetType(), typeof(Candy));
            Assert.AreEqual(DisplayOptions.ShowThankYou(), _vendingOperation.DisplayText);
            Assert.AreEqual(85,_vendingOperation.TellerAvailableFunds);
            Assert.AreEqual(10, customerPurchase.ReturnedCoins.Sum(x => x.DenominationInCents));
            Assert.AreEqual(0,_vendingOperation.CustomerAvailableFunds);
        }

        [TestMethod]
        public void int_givenExcessChangeAndAvailableProductButNotEnoughTellerFunds_CustomerWillReceiveProductAndChange()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            var customerPurchase = _vendingOperation.SelectProduct(new Candy());
            Assert.IsNull(customerPurchase);
            Assert.AreEqual(DisplayOptions.ShowExactChange(), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void int_givenNoCoinsInsertedAndProductSelected()
        {
            _vendingOperation.LoadSingleProduct(typeof(Candy));
            var customerPurchase = _vendingOperation.SelectProduct(new Candy());
            Assert.IsNull(customerPurchase);
            Assert.AreEqual(DisplayOptions.ShowPrice(new Candy().CostInCents), _vendingOperation.DisplayText);
        }

        [TestMethod]
        public void int_givenCustomerWantsCoinsReturned_CoinsAreReturned()
        {
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            _vendingOperation.CustomerInsertedCoins.Add(new Quarter());
            Assert.AreEqual(50, _vendingOperation.ReturnCustomerCoins().ReturnedCoins.Sum(x => x.DenominationInCents));
        }
    }
}
