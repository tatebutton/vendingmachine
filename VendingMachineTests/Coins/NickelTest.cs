﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class NickelTest
    {
        private Nickel _nickel;

        [TestInitialize]
        public void Initialize()
        {
            _nickel = new Nickel();
        }

        [TestMethod]
        public void GivenANickel_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(5, _nickel.DenominationInCents);
            Assert.AreEqual(5, _nickel.GramWeight);
            Assert.AreEqual(21.21, _nickel.MillimeterDiameter);
            Assert.AreEqual(1.95, _nickel.MillimeterThickness);
            Assert.AreEqual(5, _nickel.DenominationInCents);
            Assert.AreEqual("Nickel", _nickel.Name);
            Assert.IsTrue(_nickel.IsAccepted);
        }
        

    }
}
