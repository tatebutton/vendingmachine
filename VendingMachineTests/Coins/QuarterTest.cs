﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class QuarterTest
    {
        private Quarter _quarter;

        [TestInitialize]
        public void Initialize()
        {
            _quarter = new Quarter();
        }

        [TestMethod]
        public void GivenAQuarter_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(25, _quarter.DenominationInCents);
            Assert.AreEqual(5.670, _quarter.GramWeight);
            Assert.AreEqual(24.26, _quarter.MillimeterDiameter);
            Assert.AreEqual(1.75, _quarter.MillimeterThickness);
            Assert.AreEqual(25, _quarter.DenominationInCents);
            Assert.AreEqual("Quarter", _quarter.Name);
            Assert.IsTrue(_quarter.IsAccepted);
        }

    }
}
