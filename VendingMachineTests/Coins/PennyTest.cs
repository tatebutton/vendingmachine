﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class PennyTest
    {
        private Penny _penny;

        [TestInitialize]
        public void Initialize()
        {
            _penny = new Penny();
        }

        [TestMethod]
        public void GivenAPenny_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(1, _penny.DenominationInCents);
            Assert.AreEqual(2.5, _penny.GramWeight);
            Assert.AreEqual(19.05, _penny.MillimeterDiameter);
            Assert.AreEqual(1.52, _penny.MillimeterThickness);
            Assert.AreEqual(1, _penny.DenominationInCents);
            Assert.AreEqual("Penny",_penny.Name);
            Assert.IsFalse(_penny.IsAccepted);
        }
        

    }
}
