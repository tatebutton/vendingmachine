﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class DollarTest
    {
        private Dollar _dollar;

        [TestInitialize]
        public void Initialize()
        {
            _dollar = new Dollar();
        }

        [TestMethod]
        public void GivenADollar_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(100, _dollar.DenominationInCents);
            Assert.AreEqual(8.1, _dollar.GramWeight);
            Assert.AreEqual(26.49, _dollar.MillimeterDiameter);
            Assert.AreEqual(2, _dollar.MillimeterThickness);
            Assert.AreEqual(100, _dollar.DenominationInCents);
            Assert.AreEqual("Dollar", _dollar.Name);
            Assert.IsFalse(_dollar.IsAccepted);
        }
    }
}
