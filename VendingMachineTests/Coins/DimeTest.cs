﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class DimeTest
    {
        private Dime _dime;

        [TestInitialize]
        public void Initialize()
        {
            _dime = new Dime();
        }

        [TestMethod]
        public void GivenADime_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(10, _dime.DenominationInCents);
            Assert.AreEqual(2.268, _dime.GramWeight);
            Assert.AreEqual(17.91, _dime.MillimeterDiameter);
            Assert.AreEqual(1.35, _dime.MillimeterThickness);
            Assert.AreEqual(10, _dime.DenominationInCents);
            Assert.AreEqual("Dime", _dime.Name);
            Assert.IsTrue(_dime.IsAccepted);
        }

    }
}
