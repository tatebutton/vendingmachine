﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Coins;

namespace VendingMachineTests.Coins
{
    [TestClass]
    public class HalfDollarTest
    {
        private HalfDollar _halfDollar;

        [TestInitialize]
        public void Initialize()
        {
            _halfDollar = new HalfDollar();
        }

        [TestMethod]
        public void GivenAHalfDollar_CoinAttributesShouldBeValidated()
        {
            Assert.AreEqual(50, _halfDollar.DenominationInCents);
            Assert.AreEqual(11.34, _halfDollar.GramWeight);
            Assert.AreEqual(30.61, _halfDollar.MillimeterDiameter);
            Assert.AreEqual(2.15, _halfDollar.MillimeterThickness);
            Assert.AreEqual(50, _halfDollar.DenominationInCents);
            Assert.AreEqual("Half Dollar", _halfDollar.Name);
            Assert.IsFalse(_halfDollar.IsAccepted);
        }

    }
}
