﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models.Products;

namespace VendingMachineTests.Products
{
    [TestClass]
    public class ProductTest
    {
        [TestInitialize]
        public void Initialize()
        {

        }

        [TestMethod]
        public void GivenNewCandy_ObjectHasProperties()
        {
            var candy = new Candy();
            Assert.IsInstanceOfType(candy.Name, typeof(string));
            Assert.IsInstanceOfType(candy.CostInCents, typeof(int));
        }

        [TestMethod]
        public void GivenNewChips_ObjectHasProperties()
        {
            var cola = new Cola();
            Assert.IsInstanceOfType(cola.Name, typeof(string));
            Assert.IsInstanceOfType(cola.CostInCents, typeof(int));
        }

        [TestMethod]
        public void GivenNewCola_ObjectHasProperties()
        {
            var chips = new Chips();
            Assert.IsInstanceOfType(chips.Name, typeof(string));
            Assert.IsInstanceOfType(chips.CostInCents, typeof(int));
        }

    }
}
