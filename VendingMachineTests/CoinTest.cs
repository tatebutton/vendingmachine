﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachine.Models;
using VendingMachine.Models.Coins;

namespace VendingMachineTests
{
    [TestClass]
    public class CoinTest
    {
        private List<Coin> _coinItems;
        
        [TestInitialize]
        public void Initialize()
        {
            _coinItems = new List<Coin> {
                new Penny(),
                new Dime(),
                new Nickel(),
                new Quarter(),
                new HalfDollar(),
                new Dollar()};
        }

        
        [TestMethod]
        public void GivenCoinItems_WhenCoinIsPassedAWeightOfPenny_ItReturnsAPenny()
        {
            var penny = _coinItems.FirstOrDefault(x => x.GramWeight == 2.5);
            Assert.AreEqual("Penny", penny.Name);
        }

        [TestMethod]
        public void GivenCoinItems_WhenCoinIsPassedAWeightOfTwoPointFiveFiveGrams_ItReturnsNoCoin()
        {
            var coin = _coinItems.FirstOrDefault(x => x.GramWeight == 2.55);
            Assert.IsNull(coin);
        }

        [TestMethod]
        public void GivenCoinItems_WhenCoinThatIsCertainSizeAndWeightofQuarter_WillReturnCoinAndAcceptability()
        {
            var coin = _coinItems.FirstOrDefault(x => x.GramWeight == 5.670 && x.MillimeterDiameter == 24.26);
            Assert.AreEqual("Quarter", coin.Name);
            Assert.IsTrue(coin.IsAccepted);
        }

        [TestMethod]
        public void GivenCoinItems_WhenCoinThatIsCertainSizeAndWeightofPenny_WillReturnCoinAndAcceptability()
        {
            var coin = _coinItems.FirstOrDefault(x => x.GramWeight == 2.5 && x.MillimeterDiameter == 19.05);
            Assert.AreEqual("Penny", coin.Name);
            Assert.IsFalse(coin.IsAccepted);
        }


    }
}
