﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Products
{
    public class Cola : Product
    {
        public Cola()
        {
            CostInCents = 100;
            Name = "Cola";
        }

        public sealed override int CostInCents { get; set; }
        public sealed override string Name { get; set; }
    }
}
