﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Products
{
    public class Candy : Product
    {
        public Candy()
        {
            CostInCents = 65;
            Name = "Candy";
        }


        public sealed override int CostInCents { get; set; }
        public sealed override string Name { get; set; }
    }
}
