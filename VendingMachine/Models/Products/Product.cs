﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Products
{
    public abstract class Product
    {
        
        public abstract int CostInCents { get; set; }
        public abstract string Name { get; set; }
    }
}
