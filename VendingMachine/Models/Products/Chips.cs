﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Products
{
    public class Chips : Product
    {
        public Chips()
        {
            CostInCents = 50;
            Name = "Chips";
        }

        public sealed override int CostInCents { get; set; }
        public sealed override string Name { get; set; }
    }
}
