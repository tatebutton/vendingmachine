﻿namespace VendingMachine.Models
{
    public static class DisplayOptions
    {
        public static string ShowOutOfStock()
        {
            return "OUT OF STOCK";
        }

        public static string ShowExactChange()
        {
            return "EXACT CHANGE REQUIRED";
        }

        public static string ShowThankYou()
        {
            return "THANK YOU";
        }

        public static string ShowInsertCoin()
        {
            return "INSERT COIN";
        }

        public static string ShowPrice(double price)
        {
            return $"PRICE {price / 100}";
        }
    }
}
