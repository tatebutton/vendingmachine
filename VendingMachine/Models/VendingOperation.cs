﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using VendingMachine.Models.Coins;
using VendingMachine.Models.Products;

namespace VendingMachine.Models
{
    public class VendingOperation
    {
        public VendingOperation()
        {
            CapacityForEachProduct = 10;
            CustomerInsertedCoins = new List<Coin>();
            TellerCoins = new List<Coin>();
            CoinReturn = new List<Coin>();
            ProductInventory = new List<Product>();
            AvailableDenominations = new List<Coin>()
            {
                new Penny(),
                new Nickel(),
                new Dime(),
                new Quarter(),
                new HalfDollar(),
                new Dollar()
            };
            AvailableProducts = new List<Product>()
            {
                new Candy(),
                new Chips(),
                new Cola()
            };
            DisplayText = DisplayOptions.ShowInsertCoin();
        }
        public string DisplayText { get; set; }
        public List<Coin> CustomerInsertedCoins { get; set; }
        public List<Coin> TellerCoins { get; set; }
        public List<Coin> AvailableDenominations { get; set; }
        public List<Coin> CoinReturn { get; set; }
        public List<Product> AvailableProducts { get; set; }
        public List<Product> ProductInventory { get; set; }
        public int CapacityForEachProduct { get; set; }
        public int CustomerAvailableFunds => CustomerInsertedCoins.Sum(x => x.DenominationInCents);
        public int TellerAvailableFunds => TellerCoins.Sum(x => x.DenominationInCents);
        

        public void AddCustomerFunds(Coin coin)
        {
            CustomerInsertedCoins.Add(coin);
        }

        public void AddCoinToReturn(Coin coin)
        {
            CoinReturn.Add(coin);
        }
        

        public Coin ValidateCoin(InsertedCoin insertedCoin)
        {
            var coinOfAvailableDemonications = AvailableDenominations.FirstOrDefault(x => x.GramWeight == insertedCoin.WeightInGrams && x.MillimeterDiameter == insertedCoin.DiameterInMillemeters);
            if (coinOfAvailableDemonications != null)
            {
                return coinOfAvailableDemonications;
            }
            return new OtherCoin();
        }

        public void InsertCoin(InsertedCoin coin)
        {
            var validatedCoin = ValidateCoin(coin);
            if (validatedCoin.IsAccepted)
            {
                AddCustomerFunds(ValidateCoin(coin));
            }
            else 
            {
                AddCoinToReturn(validatedCoin);
            }
        }


        public void LoadAllProduct()
        {
            foreach (var availableProduct in AvailableProducts)
            {
                LoadSingleProduct(availableProduct.GetType());
            }
        }

        public void LoadSingleProduct(Type product)
        {
            var currentInventoryOfProduct = ProductInventory.Count(x => x.GetType() == product);
            for (int i = 0; i < (CapacityForEachProduct - currentInventoryOfProduct); i++)
            {
                ProductInventory.Add((Product)Activator.CreateInstance(product));
            }
        }

        public bool HasAvailableInventory(Product product)
        {
            if (ProductInventory.FirstOrDefault(x => x.GetType() == product.GetType()) != null)
            {
                return true;
            }
            DisplayText = DisplayOptions.ShowOutOfStock();
            return false;
        }

        public bool HasAdaquateFunds(Product product)
        {
            if (CustomerAvailableFunds >= product.CostInCents)
            {
                return true;
            }
            DisplayText = DisplayOptions.ShowInsertCoin();
            return false;
        }

        public Product VendProduct(Product product)
        {
            if (HasAdaquateFunds(product) && HasAvailableInventory(product))
            {
                RemoveProduct(product);
                DisplayText = DisplayOptions.ShowThankYou();
                return product;
            }
            return null;
        }

        public void RemoveProduct(Product product)
        {
            var selectedProduct = ProductInventory.FirstOrDefault(x => product.GetType() == x.GetType());
            ProductInventory.Remove(selectedProduct);
        }

        public void TransferMoney(Product product)
        {
            TellerCoins.AddRange(CustomerInsertedCoins);
            CustomerInsertedCoins.Clear();
        }

        public bool RequiresExactChange(Product product)
        {
            if (HasAdaquateFunds(product))
            {
                var remainderFunds = CalculateCustomerChange(product);
                if (remainderFunds > TellerAvailableFunds)
                {
                    DisplayText = DisplayOptions.ShowExactChange();
                    return true;
                }
                return false;
            }
            DisplayText = CustomerAvailableFunds > 0 ? DisplayOptions.ShowInsertCoin() : DisplayOptions.ShowPrice(product.CostInCents);
            
            return true;
        }

        public int CalculateCustomerChange(Product product)
        {
            return CustomerAvailableFunds - product.CostInCents;
        }

        public List<Coin> ReturnsCustomerChange(Product product)
        {
            var returnCoins = new List<Coin>();
            var changeToMake = CalculateCustomerChange(product);
            if (changeToMake > 0)
            {
                
                foreach (var coin in TellerCoins.OrderByDescending(x => x.DenominationInCents))
                {
                    if (returnCoins.Sum(x => x.DenominationInCents) + coin.DenominationInCents <= changeToMake)
                    {
                        returnCoins.Add(coin);
                    }
                }
                
            }
            TransferMoney(product);
            return returnCoins;
        }

        public CustomerPurchase ReturnCustomerCoins()
        {
            if (CustomerInsertedCoins.Any())
            {
                var ruturnedCoins = new List<Coin>();
                ruturnedCoins.AddRange(CustomerInsertedCoins);
                CustomerInsertedCoins.Clear();
                var customerPurchase = new CustomerPurchase {ReturnedCoins = ruturnedCoins};
                return customerPurchase;
            }
            return null;
        }

        public CustomerPurchase SelectProduct(Product product)
        {
            if (!RequiresExactChange(product))
            {
                var customerPurchase = new CustomerPurchase();
                var vendedProduct = VendProduct(product);
                if (vendedProduct != null)
                {
                    customerPurchase.Product = vendedProduct;
                    customerPurchase.ReturnedCoins = ReturnsCustomerChange(product);
                    return customerPurchase;
                }
            }
            return null;
        }
    }
}
