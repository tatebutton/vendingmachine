﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class Penny : Coin
    {
        public Penny()
        {
            GramWeight = 2.5;
            DenominationInCents = 1;
            Name = "Penny";
            MillimeterDiameter = 19.05;
            MillimeterThickness = 1.52;
            IsAccepted = false;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
