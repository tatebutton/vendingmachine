﻿namespace VendingMachine.Models.Coins
{
    public abstract class Coin 
    {
        public abstract double GramWeight { get; set; }
        public abstract int DenominationInCents { get; set; }
        public abstract string Name { get; set; }
        public abstract double MillimeterDiameter { get; set; }
        public abstract double MillimeterThickness { get; set; }
        public abstract bool IsAccepted { get; set; }
    }
}
