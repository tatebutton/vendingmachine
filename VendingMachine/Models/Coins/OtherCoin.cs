﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class OtherCoin : Coin
    {
        public OtherCoin()
        {
            IsAccepted = false;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
