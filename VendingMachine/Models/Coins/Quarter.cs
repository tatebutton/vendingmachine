﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class Quarter : Coin
    {
        public Quarter()
        {
            GramWeight = 5.670;
            DenominationInCents = 25;
            Name = "Quarter";
            MillimeterDiameter = 24.26;
            MillimeterThickness = 1.75;
            IsAccepted = true;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
