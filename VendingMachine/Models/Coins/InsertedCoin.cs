﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class InsertedCoin
    {
        public InsertedCoin()
        {
        }

        public InsertedCoin(double weightInGrams, double diameterInMillemeters)
        {
            WeightInGrams = weightInGrams;
            DiameterInMillemeters = diameterInMillemeters;
        }

        public double WeightInGrams { get; set; }
        public double DiameterInMillemeters { get; set; }
    }
}
