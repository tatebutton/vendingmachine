﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class Dime : Coin
    {
        public Dime()
        {
            GramWeight = 2.268;
            DenominationInCents = 10;
            Name = "Dime";
            MillimeterDiameter = 17.91;
            MillimeterThickness = 1.35;
            IsAccepted = true;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
