﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class Dollar : Coin
    {
        public Dollar()
        {
            GramWeight = 8.1;
            DenominationInCents = 100;
            Name = "Dollar";
            MillimeterDiameter = 26.49;
            MillimeterThickness = 2.0;
            IsAccepted = false;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
