﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class Nickel : Coin
    {
        public Nickel()
        {
            GramWeight = 5.0;
            DenominationInCents = 5;
            Name = "Nickel";
            MillimeterDiameter = 21.21;
            MillimeterThickness = 1.95;
            IsAccepted = true;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
