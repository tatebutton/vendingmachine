﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace VendingMachine.Models.Coins
{
    public class HalfDollar : Coin
    {
        public HalfDollar()
        {
            GramWeight = 11.340;
            DenominationInCents = 50;
            Name = "Half Dollar";
            MillimeterDiameter = 30.61;
            MillimeterThickness = 2.15;
            IsAccepted = false;
        }

        public sealed override double GramWeight { get; set; }
        public sealed override int DenominationInCents { get; set; }
        public sealed override string Name { get; set; }
        public sealed override double MillimeterDiameter { get; set; }
        public sealed override double MillimeterThickness { get; set; }
        public sealed override bool IsAccepted { get; set; }
    }
}
