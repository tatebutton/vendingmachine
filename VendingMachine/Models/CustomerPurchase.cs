﻿using System;
using System.Collections.Generic;
using System.Text;
using VendingMachine.Models.Coins;
using VendingMachine.Models.Products;

namespace VendingMachine.Models
{
    public class CustomerPurchase
    {
        public CustomerPurchase()
        {
            ReturnedCoins = new List<Coin>();
        }

        public List<Coin> ReturnedCoins { get; set; }
        public Product Product { get; set; }
    }
}
